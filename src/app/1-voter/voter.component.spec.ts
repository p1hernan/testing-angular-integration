import { TestBed, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { VoterComponent } from './voter.component';

describe('VoterComponent', () => {
  let component: VoterComponent;
  let fixture: ComponentFixture<VoterComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ VoterComponent ]
    });

    fixture = TestBed.createComponent(VoterComponent);
    component = fixture.componentInstance;
  });


  it('should render total votes', () => {
    component.othersVote = 20;
    component.myVote = 1;
    fixture.detectChanges();

    let de = fixture.debugElement.query(By.css('.vote-count'));
    let el: HTMLElement = de.nativeElement;

    expect(el.innerText).toContain('21');
  });


  it('should highlight the upvote button if I have upvoted', () => {
    component.myVote = 1;
    fixture.detectChanges();

    let de = fixture.debugElement.query(By.css('.glyphicon-menu-up'));
    expect(de.classes['highlighted']).toBeTruthy();
  });


  it('should increase total votes when the I click upvote button', () => {
    // etiqueta i que dispara el evento
    let button = fixture.debugElement.query(By.css('.glyphicon-menu-up'));
    // span donde se visualiza el resultado
    let result = fixture.debugElement.query(By.css('.vote-count'));
    let el: HTMLElement = result.nativeElement;

    button.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(el.innerText).toBe('1');
  });
});
