import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';

import { TodosComponent } from './todos.component';
import { TodoService } from './todo.service';

describe('TodosComponent', () => {
  let component: TodosComponent;
  let fixture: ComponentFixture<TodosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpModule ],
      declarations: [ TodosComponent ],
      providers: [ TodoService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // Default test
  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it("should load todos from the server", () => {
    let list = [1, 2, 3];
    let service = TestBed.get(TodoService);
    spyOn(service, "getTodos").and.returnValue(Observable.from([ list ]));

    component.ngOnInit();
    // fixture.detectChanges();

    expect(component.todos).toBe(list);
  });
});
